#!/bin/bash

if [ $UID -ne 0 ];then
	echo "ejecute el script como 'root' o 'sudo ./$0'"
	exit 1
fi

# actualizar sistema
apt update || yum update || dnf update

apt upgrade -y || yum upgrade -y || dnf upgrade -y

# cambiar hostname
read -p "hostname: " host

hostnamectl set-hostname $host

#install nginx

apt install nginx -y || apt install ngingx
